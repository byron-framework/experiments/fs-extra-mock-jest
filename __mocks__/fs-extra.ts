let state: any = {};

export function __setState(newState: any): void {
  state = newState;
}

export const existsSync = jest
  .fn((path: string): boolean => {
    const name: string = path
      .split('/')
      .reduce((acc: string, val: string): any => val);

    return Object
      .keys(state)
      .includes(name);
  });

export const mkdirSync = jest
  .fn((dirName: string): void => { state[dirName] = {} });
