import { init } from '.';

jest.mock('fs-extra');

describe('init', () => {
  describe('without arguments', () => {
    beforeEach(() => {
      process.argv = [
        'node',
        'init',
      ];
      console.log = jest.fn();
      init();
    });

    it('should print []', () => {
      expect(console.log)
        .toHaveBeenCalledWith([]);
    });
  });

  describe('with one argument', () => {
    const defaultArgs = [
        'node',
        'init',
        'Foo',
    ];

    const fs = require('fs-extra');

    describe('when directory already exists', () => {
      beforeEach(() => {
        fs.__setState({ 'Foo': {} });
        process.argv = [ ...defaultArgs ];
      });

      it('should check if directory exists', () => {
        expect(init)
          .toThrowError('Directory exists');
      });
    });

    describe('when directory does not exist', () => {
      beforeEach(() => {
        fs.__setState({});
        process.argv = [ ...defaultArgs ];
        init();
      });

      it('should call mkdirSync with the name', () => {
        expect(fs.mkdirSync)
          .toHaveBeenCalledWith('/tmp/Foo');
      });
    });
  });
});
