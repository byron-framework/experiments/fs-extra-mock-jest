import { existsSync, mkdirSync } from 'fs-extra';

export function init(): void {
  const { argv }: NodeJS.Process = process;

  const [_, __, ...args]: string[] = argv;

  if (args.length > 0) {
    const path: string = `/tmp/${argv[2]}`;
    if (!existsSync(path)) {
      mkdirSync(path);
    } else {
      throw new Error('Directory exists');
    }
  } else {
    console.log(args);
  }
}
